const std = @import("std");
const testing = std.testing;

const c = @cImport({
    @cInclude("bindings.h");
});

export fn add(a: i32, b: i32) i32 {
    return a + b;
}

export fn round_number(number: f32) f32 {
    return c.round_in_c(number);
}

export fn string_from_c() [*:0]const u8 {
    const buffer: [*c]const u8 = c.c_string();
    // Using span errors badly in the comiler
    // const ret = std.mem.span(buffer);
    // return ret;
    return buffer;
}

export fn string_from_zig() *const [14:0]u8 {
    return "Hello from zig";
}
