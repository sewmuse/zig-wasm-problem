

`zig build` yeilds

```
zig build-lib test Debug wasm32-freestanding: error: warning(link): unexpected LLD stderr:
wasm-ld: warning: function signature mismatch: c_string
>>> defined as (i32) -> i32 in /home/gap/temp/zig_wasm/zig-cache/o/12a4e7a2145498c3a4aa83fe910529d8/test.wasm.o
>>> defined as () -> i32 in /home/gap/temp/zig_wasm/zig-cache/o/085d0386b639fd727f0fd20ecbeb50cc/bindings.o
```

`wasmtime zig-out/lib/test.wasm --invoke string_from_zig` yeilds (Calling the function from wasm_time ):

```
Error: failed to run main module `zig-out/lib/test.wasm`

Caused by:
    0: failed to invoke `string_from_c`
    1: error while executing at wasm backtrace:
           0:   0x8f - <unknown>!signature_mismatch:c_string
           1:  0x23b - <unknown>!string_from_c
       note: using the `WASMTIME_BACKTRACE_DETAILS=1` environment variable may show more debugging information
    2: wasm trap: wasm `unreachable` instruction executed
```

