const std = @import("std");

pub fn build(b: *std.Build) void {
    const cflags = [_][]const u8{""};

    const lib = b.addSharedLibrary(.{
        .name = "test",
        .root_source_file = .{ .path = "src/main.zig" },
        .target = .{ .cpu_arch = .wasm32, .os_tag = .freestanding },
        .optimize = b.standardOptimizeOption(.{}),
    });

    lib.linkLibC();
    lib.addIncludePath(.{ .path = "src" });
    lib.addCSourceFile(.{ .file = std.Build.FileSource.relative("src/bindings.c"), .flags = &cflags });

    // export all functions marked with "export"
    lib.rdynamic = true;
    b.installArtifact(lib);
}
